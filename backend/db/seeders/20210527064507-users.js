'use strict';
const bcrypt = require('bcrypt');
const saltRounds = 10;

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   await queryInterface.bulkInsert('Users',[
     {
        firstName: "Samuel",
        lastName: "Barberena",
        email: "samuelbarberena12@gmail.com",
        password: await hashPassword('password'),
        createdAt: new Date(),
        updatedAt: new Date(),
     }
   ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Users', null, {});
  }
};

async function hashPassword(password) {
  const passwordHash = await bcrypt.hash(password, saltRounds);
  return passwordHash;
}
