"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert('Probabilities', [
      {
        level: "Altamente improbable",
        weight: 1,
        porcent: 0.2,
        description:
          "Ocurre solamente en circunstancias excepcionales. Los controles de seguridad existentes son seguros y hasta han suministrado un adecuado nivel de protección.",
        organizationId: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        level: "Improbable",
        weight: 2,
        porcent: 0.4,
        description:
          "Podría ocurrir en algún momento, Los controles de seguridad existentes son moderados y en general han suministrado un adecuado nivel de protección.",
        organizationId: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        level: "Eventual",
        weight: 3,
        porcent: 0.6,
        description:
          "Es posible la ocurrencia de nuevos incidentes, pero no muy probable.",
        organizationId: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        level: "Probable",
        weight: 4,
        porcent: 0.8,
        description:
          "Ocurre normalmente. Existe una gran probabilidad de que haya incidentes así en el futuro.",
        organizationId: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        level: "Altamente probable",
        weight: 5,
        porcent: 1,
        description:
          "Se espera que ocurra en la mayoría de las circunstancias. Los controles de seguridad existentes son bajos o ineficaces.",
        organizationId: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Probabilities', null, {})
  },
};
