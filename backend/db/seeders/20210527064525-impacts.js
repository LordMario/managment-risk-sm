'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   await queryInterface.bulkInsert('Impacts', [
     {
       level: 'Insignificante',
       weight: 1,
       porcent: 0.2,
       description: 'Pérdida económica que no pone en riesgo los intereses de la compañía y no afecta el flujo normal de los procesos.',
      organizationId: 1,
       createdAt: new Date(),
      updatedAt: new Date(),
      
     },
     {
      level: 'Menor',
      weight: 2,
      porcent: 0.4,
      description: 'Pérdida económicamente asumible por la compañía sin consecuencias ni esfuerzos adicionales que afecten notablemente su situación financiera y no afecta los procesos de manera considerable.',
      organizationId: 1,
      createdAt: new Date(),
      updatedAt: new Date(),
      
    },
    {
      level: 'Moderado',
      weight: 3,
      porcent: 0.6,
      description: 'Pérdida económicamente mediana, respaldable por la compañía y puede afectar el flujo normal de algún proceso de la compañía.',
      organizationId: 1,
      createdAt: new Date(),
      updatedAt: new Date(),
      
    },
    {
      level: 'Mayor',
      weight: 4,
      porcent: 0.8,
      description: 'Pérdida económica importante, que implica esfuerzos adicionales no planeados por la compañía y se puede presentar una interrupción parcial en los procesos.',
      organizationId: 1,
      createdAt: new Date(),
      updatedAt: new Date(),
      
    },
    {
      level: 'Catastrófico',
      weight: 5,
      porcent: 1,
      description: 'Pérdida económica que compromete seriamente el       patrimonio y la estabilidad de la compañía y se interrumpe el proceso normal de las operaciones de manera indefinida.',
      organizationId: 1,
      createdAt: new Date(),
      updatedAt: new Date(),
      
    },
   ])
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Users', null, {});
  }
};