
const { DataTypes } = require("sequelize");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Risks', {
      id: {
        primaryKey: true, 
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false
      },
      code: {
        allowNull: true,
        type: DataTypes.STRING(5),
      },
      name: {
        allowNull: false,
        type: DataTypes.STRING
      },
      description: {
        allowNUll: false,
        type: DataTypes.TEXT
      },
      impact: {
        allowNull: false,
        type: DataTypes.INTEGER
      },
      probability: {
        allowNull: false,
        type: DataTypes.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
     await queryInterface.dropTable('Risks');
  }
};
