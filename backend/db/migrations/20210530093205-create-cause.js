
const { DataTypes } = require("sequelize");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Causes', {
      id: {
        primaryKey: true, 
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false
      },
      causeDescription:{
        allowNull: false,
        type: DataTypes.TEXT
      },
      riskId: {
        allowNull: false,
        type: DataTypes.INTEGER,
        references: {
          model: {tableName: 'Risks'},
          key: 'id'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
     await queryInterface.dropTable('Causes');
  }
};
