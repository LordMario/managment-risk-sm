'use strict';

const { DataTypes } = require("sequelize");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
     await queryInterface.createTable('Risk_Control', {
      riskId: {
        allowNull: false,
        type: DataTypes.INTEGER,
        references:{
          model: {tableName: 'Risks'},
          key: 'id'
        }
      },
      controlId: {
        allowNull: false,
        type: DataTypes.INTEGER,
        references:{
          model: {tableName: 'Controls'},
          key: 'id'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
     await queryInterface.dropTable('Risk_Control', {});
  }
};
