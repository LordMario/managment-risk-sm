const { DataTypes } = require("sequelize");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Impacts', {
      id: {
        primaryKey: true, 
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false
      },
      level: {
        allowNull: false,
        type: DataTypes.STRING
      },
      weight: {
        allowNull: false,
        type: DataTypes.INTEGER
      },
      porcent: {
        allowNull:true,
        type: DataTypes.DOUBLE
      },
      description: {
        allowNull: true,
        type: DataTypes.TEXT
      },
      organizationId: {
        allowNull: false,
        type: DataTypes.INTEGER,
        references: {
          model: {tableName: 'Organizations'},
          key: 'id'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
     await queryInterface.dropTable('Impacts');
  }
};
