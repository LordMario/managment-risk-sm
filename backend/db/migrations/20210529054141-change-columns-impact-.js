'use strict';

const { DataTypes } = require("sequelize");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.renameColumn('Risks', 'impact', 'impactId',{
      allowNull: false, 
      type: DataTypes.INTEGER,
      references: {
        model: {tableName: 'Impacts'},
        key: 'id'
      }
    } );
    await queryInterface.renameColumn('Risks', 'probability', 'probabilityId', {
      allowNull: false, 
      type: DataTypes.INTEGER,
      references: {
        model: {tableName: 'Probabilities'},
        key: 'id'
      }
    });
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
