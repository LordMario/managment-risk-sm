
const { DataTypes } = require("sequelize");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Organizations', {
      id: {
        primaryKey: true, 
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false
      },
      organizationName: {
        allowNull: false,
        type: DataTypes.STRING
      },
      address: {
        allowNull: true,
        type: DataTypes.TEXT
      },
      industry:{
        allowNull: true,
        type: DataTypes.STRING
      },
      mission: {
        allowNull: true,
        type: DataTypes.TEXT
      },
      vision: {
        allowNull: true,
        type: DataTypes.TEXT
      },
      userId: {
        allowNull: false,
        type: DataTypes.INTEGER,
        references:{
          model: {tableName: 'Users'},
          key: 'id'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
     await queryInterface.dropTable('Organizations');
  }
};
