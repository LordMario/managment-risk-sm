'use strict';

const { DataTypes } = require("sequelize");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
     await queryInterface.createTable('Controls', {
      id: {
        primaryKey: true, 
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false
      },
      nombre: {
        allowNull: false,
        type: DataTypes.STRING
      },
      description:{
        allowNull: false,
        type: DataTypes.TEXT
      },
      frequency:{
        allowNull: false,
        type: DataTypes.INTEGER
      },
      compliance:{
        allowNull: false,
        type: DataTypes.INTEGER
      },
      documentation:{
        allowNull: false,
        type: DataTypes.INTEGER        
      },
      resposible:{
        allowNull: false,
        type: DataTypes.INTEGER        
      },
      evidence:{
        allowNull: false,
        type: DataTypes.INTEGER        
      },
      efficiency:{
        allowNull: false,
        type: DataTypes.DOUBLE        
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    }); 
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
     await queryInterface.DropTable('Controls', {});
  }
};
