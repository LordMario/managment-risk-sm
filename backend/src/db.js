const { Sequelize } = require('sequelize');
const config = require("../config/config.json");

const hostname = process.env.MYSQL_ADDON_HOST;
const db = process.env.MYSQL_ADDON_DB;
const user = process.env.MYSQL_ADDON_USER;
const password = process.env.MYSQL_ADDON_PASSWORD;

const sequelize = new Sequelize('mrisk', "root", '',{
    host: 'localhost',
    dialect: 'mysql'
});

try{
    sequelize.authenticate();
    console.log('Connection has been established successfully.');
}catch (error){
    console.log('Unable to connect to the database:', error);
}

module.exports = sequelize;
