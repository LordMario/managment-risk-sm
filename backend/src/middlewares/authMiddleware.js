const jwt = require('jsonwebtoken');

const auth = (req, res, next) => {
    const accessToken = req.headers['authorization'];
    if(!accessToken) res.status(500).send('access denied')

    jwt.verify(accessToken, process.env.SECRET, (err, user)=>{
        if(err){
            res.status(500).send('Acess denied, token expired or  incorrect');
        }else{
            next();
        }
    });
}

module.exports = auth;
