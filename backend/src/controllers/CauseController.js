
const Cause = require('../models/Cause');
const Risk = require('../models/Risk');
const {validationCauseSchema} = require('../validations/causeEffectValidation');

module.exports = {
    index: async (req, res)=>{
        //
    },
    show: async (req, res)=>{
        //
    },
    create: async (req, res)=>{
        try {
            const data = req.body;

            const {error} = validationCauseSchema(data);
            if(error) return res.json({"status": 400, "error": error});

            const riskExist = await Risk.findByPk(data.riskId);
            if(!riskExist) return res.json({"status": 404, "message": "risk not register yet!"});

            const cause = await Cause.build(data);

            cause.save().then(()=>{
                res.json({"status": 200, "message": "cause created successfully!"});
            }).catch((err)=>{
                res.json({"status": 500, "message": "error updating cause"});
            });
            
        } catch (err) {
            console.log(err);
            res.json({"status": 500, "message": "somethis broke!"});
        }
    },
    update: async (req, res)=>{
        try {
            const description = req.body.causeDescription;
            if(!description) return res.json({"status": 400, "error": "description es required"});

            const { id } = req.params;

            const cause = await Cause.findByPk(id);
            if(!cause) return res.json({"status": 404, "message": "effect not register yet!"});

            cause.causeDescription = description;

            cause.save().then(()=>{
                res.json({"status": 200, "message": "cause updated successfully!"});
            }).catch((err)=>{
                res.json({"status": 500, "message": "error updating cuase"});
            });
            
        } catch (err) {
            console.log(err);
            res.json({"status": 500, "message": "somethis broke!"});
        }
    },
    getCauses: async (req, res)=>{
        try {
            const { id } = req.params;

            const riskExist = await Risk.findByPk(id);
            if(!riskExist) return res.json({"status": 404, "message": "risk not register yet!"});

            const causes = await Cause.findAll({where: {riskId: id}});

            if(!causes.length > 0) return res.json({"status": 200, "message": "not match found"});

            res.json({
                "status": 200, 
                "message": "list of causes",
                "count": causes.length,
                "data": causes
            });
            
        } catch (err) {
            console.log(err);
            res.json({"status": 500, "message": "somethis broke!"});
        }
    },
    delete: async (req, res)=>{
        //
    }
}
