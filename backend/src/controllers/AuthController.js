const User = require('../models/User');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const Organization = require('../models/Organization');
const saltRounds = 10;

module.exports = {
    login: async (req, res)=>{
        try {
            const {email, password} = req.body;

            console.log("Data request:", email);

            const user = await User.findOne({
                where: { email: email }
            })

            if(user == null) return res.status(404).json({"status": 404,"message": "user not found"});

            const validPass =  await bcrypt.compare(password, user.password);

            if (!validPass) return res.status(400).json({"status": 400,"message": "email or password incorrect"});

            const organization = await Organization.findOne({where: {userId: user.id}});

            const userAuth = {
                "id": user.id,
                "firstName": user.firstName,
                "lastName": user.lastName,
                "email": user.email,
                "organizationId": organization ? organization.id : null
            }
            const accessToken = generateAccessToken(userAuth);

            res.status(200).header('authorization', accessToken).json({
                "status": 200,
                "message": "login successfully",
                "token": accessToken,
                "user": {
                    userAuth
                }
            });

        } catch (e) {
            console.log(e);
            res.status(500).send('something broke!')
        }
    },
    signup: async (req, res)=>{
        try {
            const {password, email} = req.body;    
            
            console.log(req.body)

            const userExits = await User.findOne({where: { email: email }})
            if(userExits != null) return res.json({"status": 400, "message": 'email exists already'});

            const passwordHash = await bcrypt.hash(password, saltRounds);
            const user = await User.build(req.body);
            user.password = passwordHash;
            
            user.save().then(()=>{
                res.status(200).json({
                    "status": 200, 
                    "message": 'user register successfully!', 
                });
            }).catch((err)=>{
                console.log(err)
                res.status(400).json({"status": 400, "message": 'something broke!'});
            })

        } catch (e) {
            console.log(e)
            res.status(500).send('something broke!')
        }

    }
}

function generateAccessToken(user) {
    return jwt.sign(user, process.env.SECRET, {expiresIn: '24h'});
}
