const Organization = require("../models/Organization");
const Impact = require("../models/parametization/Impact");
const Probability = require("../models/parametization/Probability");
const User = require("../models/User");
const {
  validationOrganizationSchema,
  validationOrganizationUpdateSchema,
} = require("../validations/organizationValidation");

module.exports = {
  index: async (req, res) => {
    try {
      const organizations = await Organization.findAll();

      if (!organizations)
        return res
          .status(200)
          .json({ status: 200, message: "no registered organizations yet" });

      res.status(200).json({
        status: 200,
        message: "all organization",
        register: organizations.length,
        data: organizations,
      });
    } catch (err) {
      console.log(err);
      res.status(500).json({ status: 500, message: "something broke!" });
    }
  },
  show: async (req, res) => {
    try {
      const { id } = req.params;
      const organization = await Organization.findOne({
        where: {
          id: id,
        },
        attributes: {exclude: ['UserId']},
        include: {
          model: User,
          attributes: ["firstName", "lastName", "email"],
        },
      });

      if (!organization)
        return res
          .status(404)
          .json({ status: 404, message: "no matches found" });

      res.status(200).json({
        status: 200,
        message: "a match found",
        data: organization,
      });
    } catch (err) {
      console.log(err);
      res.status(500).json({ status: 500, message: "something broke!" });
    }
  },
  create: async (req, res) => {
    try {
      const data = req.body;

      const { error } = validationOrganizationSchema(data);
      if (error) return res.status(400).json({ status: 400, error: error });

      const userExist = await User.findByPk(data.userId);
      if(!userExist) return res.json({ "status": 404, "message": "user not exist" }); 

      const organizationExist = await Organization.findOne({where: {userId: data.userId}});
      if(organizationExist) return res.json({ "status": 200, "message": "user have organization" }); 

      const organization = await Organization.build(data);

      organization
        .save()
        .then((response) => {
          res
            .status(200)
            .json({
              status: 200,
              message: "organization created successfully",
              data: {
                "organizationId": response.dataValues.id
              },
            });
        })
        .catch((err) => {
          console.log(err);
          res
            .status(500)
            .json({ status: 500, message: "error creating organization" });
        });

    } catch (err) {
      console.log(err);
      res.status(500).send("something broke!");
    }
  },
  update: async (req, res) => {
    try {
      const data = req.body;

      const { error } = await validationOrganizationSchema(data);
      if (error) return res.status(400).json({ status: 400, error: error });

      const userExist = await User.findByPk(data.userId);
      if(!userExist) return res.json({ "status": 404, "message": "user not exist" }); 

      const { id } = req.params;
      const organization = await Organization.findByPk(id);
      if (!organization)
        return res
          .status(404)
          .json({ status: 404, message: "this organization does not exist" });

      organization.organizationName = data.organizationName;
      organization.address = data.address;
      organization.industry = data.industry;
      organization.mission = data.mission;
      organization.vision = data.vision;

      organization
        .save()
        .then(() => {
          res
            .status(200)
            .json({
              status: 200,
              message: "organization updated successfully",
            });
        })
        .catch((err) => {
          console.log(err);
          res
            .status(500)
            .json({ status: 500, message: "error updated organization" });
        });
    } catch (err) {
      console.log(err);
      res.status(500).send("something broke!");
    }
  },
  getParametizationType: async (req, res) => {
    try {
      const { id, type } = req.params;

      const query = {
        where: { organizationId: id },
        /* include: {
          model: Organization,
        }, */
      };

      if (type === "impacts") {
        parametizationType = await Impact.findAll(query);
      } else if (type === "probabilities") {
        parametizationType = await Probability.findAll(query);
      } else {
        res.json({ status: 404, message: "path not found" });
      }

      if(parametizationType.length == 0) return res.status(404).json({"status": 404, "message": "no matches found"});

      res.status(200).json({
        status: 200,
        message: "a match found",
        count: parametizationType.length,
        data: parametizationType,
      });

    } catch (err) {
      console.log(err);
      res.status(500).json({ status: 500, message: "something broke!" });
    }
  },
  getParametization: async (req, res) => {
    try {
      const { id } = req.params;

      let query = {
        attributes: ['id', 'level', 'weight', 'description'],
        where: { organizationId: id },
        /* include: {
          model: Organization,
        }, */
      };

      impacts = await Impact.findAll(query);
      if(impacts.length == 0){
        query.where.organizationId = 1;
        impacts = await Impact.findAll(query);
      }

      probabilities = await Probability.findAll();
      if(probabilities.length == 0){
        query.where.organizationId = 1
        probabilities = await Probability.findAll(query);
      }

      res.status(200).json({
        status: 200,
        message: "a match found",
        data: {
          "Impacts": impacts,
          "Probabilities": probabilities
        },
      });

    } catch (err) {
      console.log(err);
      res.status(500).json({ status: 500, message: "something broke!" });
    }
  },
  delete: async (req, res) => {
    //
  },
};
