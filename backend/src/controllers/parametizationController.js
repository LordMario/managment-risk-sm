const Impact = require('../models/parametization/Impact');
const Probability = require('../models/parametization/Probability');
const Organization = require('../models/Organization');
const { validationParametizationSchema } = require('../validations/parameterizationValidation');
const paramDefaults = require("../../db/seeders/parametizationDefault.json");

module.exports = {
    index: async (req, res)=>{
        try{
            const { type } = req.params;
            let parametization;

            if(type === 'impacts'){
                parametization = await Impact.findAll({where:{organizationId:1}})
            }else if(type === 'probabilities'){
                parametization = await Probability.findAll({where:{organizationId:1}});
            }else{
                res.json({"status": 404, "message": "path not found"});
            }

            if(parametization.length > 0){
                res.json({
                    "status": 200,
                    "message": `default ${type} list`,
                    "items": parametization.length,
                    "data": parametization
                });
           }else{
               res.json({
                   "status": 200,
                   "message": `no registered ${type} yet`
               });
           }

        }catch(err){
            console.log(err);
            res.json({"status": 500, "message": "something broke!"});
        }
    },
    create: async (req, res)=>{
        try{
            const { type } = req.params   

            if((type != 'impacts') && (type != 'probabilities')) return res.json({"status": 404, "message": "path not found"});
               
            const data = req.body;

            const { error } = validationParametizationSchema(data);
            if(error) return res.json({"status": 400, "error": error});
            
            const organization = await Organization.findByPk(data.organizationId);
            if(!organization) return res.json({"status": 404, "message": "organization not exit"});

            let parametization;

            if(type === 'impacts'){
                parametization = await Impact.build(data)
            }else if(type === 'probabilities'){
                parametization = await Probability.build(data);
            }

            parametization.save().then(()=>{
                res.json({"status": 200, "message": `${type} created successfully!`});
            }).catch((err)=>{
                console.log(err);
                res.json({"status": 500, "message": `error to create "${type}"!`, "error": err });
            });

        }catch(err){
            console.log(err);
            res.json({"status": 500, "message": "something broke!"});
        }
    },
    update: async (req, res)=>{
        res.json({"status": 500, "message": "Joven este endpoint apenas esta siendo construido, sea paciente y espere hasta que este listo!!!"});
    },
    getParametizationDefault: async (req, res)=>{
        try{
            if(Object.keys(paramDefaults).length != 0){
                res.json({
                    "status": 200, 
                    "message": "parametization defaults",
                    "data": paramDefaults
                });
            }else{
                res.json({"status": 400, "message": "error getting parametization defaults"});
            }
        }catch(err){
            console.log(err);
            res.json({"status": 500, "message": "something broke!"});
        }
    },
    createParametizationDefault: async (req, res)=>{
        try{
            if(Object.keys(paramDefaults).length == 0) return res.json({"status": 400, "message": "error getting parametization defaults"});

            const { id } = req.params;
            const { Impacts, Probabilities } = paramDefaults;

            const organization = await Organization.findByPk(id);
            if(!organization) return res.json({"status": 404, "message": "organization not exist!"});

            //create impacts
            const impactsExits = await Impact.findAll({where: {organizationId: id}});
            const probabilitiesExits = await Probability.findAll({where: {organizationId: id}});

            if(!impactsExits && !probabilitiesExits){
                if(impactsExits){
                    Impacts.forEach((impact)=>{
                        impact['organizationId'] = id;
                        impact['createdAt'] = new Date();
                        impact['creupdatedAtatedAt'] = new Date();
                        const ImpactTemp = Impact.build(impact);
                        ImpactTemp.save();
                    });
                }            
    
                //create impacts
                Probabilities.forEach((probability)=>{
                    probability['organizationId'] = id;
                    probability['createdAt'] = new Date();
                    probability['creupdatedAtatedAt'] = new Date();
                    const ProbabilityTemp = Probability.build(probability);
                    ProbabilityTemp.save();
                });
    
                res.json({
                    "status": 200, 
                    "message": `impacts and probabilities are created to organozation with id ${id}`,
                });
            }else{
                res.json({
                    "status": 200, 
                    "message": `this organization have parametization`,
                });
            }


        }catch(err){
            console.log(err);
            res.json({"status": 500, "message": "something broke!"});
        }
    }
}
