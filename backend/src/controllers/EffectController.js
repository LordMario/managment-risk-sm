
const Cause = require('../models/Cause');
const Effect= require('../models/Effect');
const Risk = require('../models/Risk');
const {validationEffectSchema} = require('../validations/causeEffectValidation');

module.exports = {
    index: async (req, res)=>{
        //
    },
    show: async (req, res)=>{
        //
    },
    create: async (req, res)=>{
        try {
            const data = req.body;

            const {error} = validationEffectSchema(data);
            if(error) return res.json({"status": 400, "error": error});

            const riskExist = await Risk.findByPk(data.riskId);
            if(!riskExist) return res.json({"status": 404, "message": "risk not register yet!"});

            const cause = await Effect.build(data);

            cause.save().then(()=>{
                res.json({"status": 200, "message": "effect created successfully!"});
            }).catch((err)=>{
                res.json({"status": 500, "message": "error updating effect"});
            });
            
        } catch (err) {
            console.log(err);
            res.json({"status": 500, "message": "somethis broke!"});
        }
    },
    update: async (req, res)=>{
        try {
            const description = req.body.effectDescription;
            if(!description) return res.json({"status": 400, "error": "description es required"});

            const { id } = req.params;

            const effect = await Effect.findByPk(id);
            if(!effect) return res.json({"status": 404, "message": "effect not register yet!"});

            effect.effectDescription = description;

            effect.save().then(()=>{
                res.json({"status": 200, "message": "effec updated successfully!"});
            }).catch((err)=>{
                res.json({"status": 500, "message": "error updating cuase"});
            });
            
        } catch (err) {
            console.log(err);
            res.json({"status": 500, "message": "somethis broke!"});
        }
    },
    getEffects: async (req, res)=>{
        try {
            const { id } = req.params;

            const riskExist = await Risk.findByPk(id);
            if(!riskExist) return res.json({"status": 404, "message": "risk not register yet!"});

            const effect = await Effect.findAll({where: {riskId: id}});

            if(!effect.length > 0) return res.json({"status": 200, "message": "not match found"});

            res.json({
                "status": 200, 
                "message": "cause created successfully!",
                "count": effect.length,
                "data": effect
            });
            
        } catch (err) {
            console.log(err);
            res.json({"status": 500, "message": "somethis broke!"});
        }
    },
    delete: async (req, res)=>{
        //
    }
}
