
const User = require('../models/User');

module.exports = {
    index: async (req, res)=>{
        const users = await User.findAll();
        res.json(users);
    },
    show: async (req, res)=>{
        //
    },
    create: async (req, res)=>{
        //
    },
    update: async (req, res)=>{
        //
    },
    delete: async (req, res)=>{
        //
    }
}
