const Organization = require('../models/Organization');
const Impact = require('../models/parametization/Impact');
const Probability = require('../models/parametization/Probability');
const Risk = require('../models/Risk');
const {validationRiskSchema, validationRiskUpdateSchema} = require("../validations/riskValidation");

module.exports = {
    index: async (req, res)=>{
        res.json('list');
    },
    show: async (req, res)=>{
        //
    },
    create: async (req, res)=>{
        try {
            const data = req.body;

            const {error} = validationRiskSchema(data);
            if(error) return res.json({"status": 400, "error": error});

            const organization = await Organization.findByPk(data.organizationId);
            if (!organization) return res.json({"status": 404, "message": "organzation not exits!"});

            const risk = Risk.build(data);            

            risk.save().then((response)=>{
                risk.code = generateCodeRisk(response.dataValues.id);
                risk.save();
                res.status(200).json({
                    "status": 200,
                    "message": "risk created successfully!"
                });
            }).catch((err)=>{
                console.log(err)
                return res.jsson({"status": 500, "message:": "error creating risk!"});
            });

        } catch (err) {
            console.log(err)
            res.jsson({"status": 500, "message:": "something broke!"});
        }        
    },
    update: async (req, res)=>{
        try {
            const { id } = req.params;
            const data = req.body;

            const {error} = validationRiskUpdateSchema(data);
            if(error) return res.json({"status": 400, "error": error});

            const risk = await Risk.findByPk(id);
            if (!risk) return res.json({"status": 404, "message": "risk not exits!"});
            
            risk.name = data.name;
            risk.description = data.description;
            risk.impactId = data.impactId;
            risk.probabilityId = data.probabilityId;

            risk.save().then((response)=>{
                risk.code = generateCodeRisk(response.dataValues.id);
                risk.save();
                res.status(200).json({
                    "status": 200,
                    "message": "risk update successfully!"
                });
            }).catch((err)=>{
                console.log(err)
                return res.jsson({"status": 500, "message:": "error updating risk!"});
            });

        } catch (err) {
            console.log(err)
            res.jsson({"status": 500, "message:": "something broke!"});
        }
    },
    getRisk: async (req, res)=>{
        try{
            const { id } = req.params;

            let risks = await Risk.findAll({
                where: {
                    organizationId: id
                },
                include: [
                    {
                        model: Probability
                    },
                    {
                        model: Impact
                    }
                ]
            });

            if(risks.length == 0) return res.json({"status": 200, "message": "not match found"});

            res.json({
                "status": 200,
                "message": "list of risk",
                "count": risks.length,
                "data": risks
            });

        }catch(err){
            console.log(err);
            res.json({"status": 500, "message": "something broke!"});
        }
    },
    test: (req, res)=>{
        res.json('test');
    },
    delete: async (req, res)=>{
        //
    }
}

function generateCodeRisk(id) {
    if(id <= 9) return 'R000'+id;
    if(id <= 99) return 'R00'+id;
    if(id <= 999) return 'R0'+id;
    //if(id <= 9999) return 'R'+id
}
