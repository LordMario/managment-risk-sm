const Joi = require("joi");

module.exports = {
    validationOrganizationSchema: (data) => {

        const schema = Joi.object().keys({
            organizationName: Joi.string().min(3).required(),
            address: Joi.string().min(10).required(),
            industry: Joi.string(),
            mission: Joi.string(),
            vision: Joi.string(),
            contactNumber: Joi.string().min(8).max(12),
            userId: Joi.number().required()
        }).options({
            abortEarly: false
        });
    
        return schema.validate(data);
    
    },    
    /* validationOrganizationUpdateSchema: (data) => {
        const schema = Joi.object().keys({
            organizationName: Joi.string().min(3).required(),
            address: Joi.string().min(10).required(),
            industry: Joi.string(),
            mission: Joi.string(),
            vision: Joi.string()
        }).options({
            abortEarly: false
        });    
    
        return schema.validate(data);
    } */
}