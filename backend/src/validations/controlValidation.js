const Joi = require('joi');

module.exports = {
    validationControlSchema: (data)=>{
        const schema = Joi.object().keys({
            nombre: Joi.string().required(),
            description: Joi.string().required(),
            frequency: Joi.number().integer().required(),
            compliance: Joi.number().integer().required(),
            documentation: Joi.number().integer().required(),
            responsible: Joi.number().integer().required(),
            evidence: Joi.number().integer().required(),
            riskId: Joi.number().integer().required(),
        }).options({
            abortEarly: false
        });
        return schema.validate(data);
    }

}