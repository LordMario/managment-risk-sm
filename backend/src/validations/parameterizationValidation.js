const Joi = require('joi');

module.exports = {

    validationParametizationSchema: (data)=>{
        const schema = Joi.object().keys({
            level: Joi.string().required(),
            weight: Joi.number().integer().positive().required(),
            description: Joi.string().required(),
            organizationId: Joi.number().integer().positive().required()
        }).options({
            abortEarly: false
        });
        return schema.validate(data);
    }

}