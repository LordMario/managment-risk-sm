const Joi = require("joi");

module.exports = {
    validationCauseSchema: (data)=>{
        const schema = Joi.object().keys({
            causeDescription: Joi.required(),
            riskId: Joi.number().positive().required()
        });
        return schema.validate(data);
    },
    validationEffectSchema: (data)=>{
        const schema = Joi.object().keys({
            effectDescription: Joi.required(),
            riskId: Joi.number().positive().required()
        });
        return schema.validate(data);
    }
}