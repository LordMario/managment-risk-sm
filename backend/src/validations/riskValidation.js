const Joi = require("joi");

module.exports = {

    validationRiskSchema: (data)=>{
        const schema = Joi.object().keys({
            name: Joi.string().min(5).required(),
            description: Joi.string().min(5).required(),
            impactId: Joi.number().integer().positive().required(),
            probabilityId: Joi.number().integer().positive().required(),
            organizationId: Joi.number().integer().positive().required()
        }).options({
            abortEarly: false
        });

        return schema.validate(data);
    },
    validationRiskUpdateSchema: (data)=>{
        const schema = Joi.object().keys({
            name: Joi.string().min(5).required(),
            description: Joi.string().min(5).required(),
            impactId: Joi.number().integer().positive().required(),
            probabilityId: Joi.number().integer().positive().required()
        }).options({
            abortEarly: false
        });

        return schema.validate(data);
    }

}