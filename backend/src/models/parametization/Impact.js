const { Sequelize, DataTypes } = require("sequelize");
const sequilize = require("../../db");
const Organization = require("../Organization");

const Impact = sequilize.define("Impact", {
  id: {
    primaryKey: true, 
    autoIncrement: true,
    type: DataTypes.INTEGER,
    allowNull: false
  },
  level: {
    allowNull: false,
    type: DataTypes.STRING
  },
  weight: {
    allowNull: false,
    type: DataTypes.INTEGER
  },
  porcent: {
    allowNull:true,
    type: DataTypes.DOUBLE
  },
  description: {
    allowNull: true,
    type: DataTypes.TEXT
  },
  organizationId: {
    allowNull: false,
    type: DataTypes.INTEGER,
    references: {
      model: {tableName: 'Organizations'},
      key: 'id'
    }
  },
  createdAt: {
    allowNull: false,
    type: Sequelize.DATE
  },
  updatedAt: {
    allowNull: false,
    type: Sequelize.DATE
  }
});

//Impact.belongsTo(Organization); TODO check asociation
//Organization.hasMany(Impact); //TODO check asociation

module.exports = Impact;
