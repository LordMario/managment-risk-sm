const { Sequelize, DataTypes } = require("sequelize");
const sequilize = require("../../db");
const Organization = require("../Organization");

const Probability = sequilize.define("Probability", {
  level: {
    allowNull: false,
    type: DataTypes.STRING,
  },
  weight: {
    allowNull: false,
    type: DataTypes.INTEGER,
  },
  organizationId: {
    allowNull: false,
    type: DataTypes.INTEGER,
    references: {
      model: {tableName: 'Organizations'},
      key: 'id'
    }
  },
  porcent: {
    allowNull: true,
    type: DataTypes.DOUBLE,
  },
  description: {
    allowNull: true,
    type: DataTypes.TEXT,
  },
});

//Probability.belongsTo(Organization); TODO check asociation
//Organization.hasMany(Probability); TODO check asociation

module.exports = Probability;
