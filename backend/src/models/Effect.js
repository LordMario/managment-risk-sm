const { Sequelize, DataTypes } = require("sequelize");
const sequilize = require("../db");

const Effect = sequilize.define("Effect", {
  id: {
    primaryKey: true,
    autoIncrement: true,
    type: DataTypes.INTEGER,
    allowNull: false,
  },
  effectDescription: {
    allowNull: false,
    type: DataTypes.TEXT,
  },
  riskId: {
    allowNull: false,
    type: DataTypes.INTEGER,
    references: {
      model: { tableName: "Effect" },
      key: "id",
    },
  },
  createdAt: {
    allowNull: false,
    type: Sequelize.DATE,
  },
  updatedAt: {
    allowNull: false,
    type: Sequelize.DATE,
  },
});

module.exports = Effect;
