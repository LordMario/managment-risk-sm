const { Sequelize, DataTypes } = require("sequelize");
const sequelize = require("../db");
const Organization = require("../models/Organization");
const Impact = require("./parametization/Impact");
const Probability = require("./parametization/Probability");

const Risk = sequelize.define("Risk", {
  id: {
    primaryKey: true,
    autoIncrement: true,
    type: DataTypes.INTEGER,
    allowNull: false,
  },
  code: {
    allowNull: true,
    type: DataTypes.STRING(5),
  },
  name: {
    allowNull: false,
    type: DataTypes.STRING,
  },
  description: {
    allowNUll: false,
    type: DataTypes.TEXT,
  },
  impactId: {
    allowNull: false, 
    type: DataTypes.INTEGER,
    references: {
      model: {tableName: "Impacts"},
      key: "id"
    }
  },
  probabilityId: {
    allowNull: false, 
      type: DataTypes.INTEGER,
      references: {
        model: {tableName: 'Probabilities'},
        key: 'id'
      }
  },
  organizationId:{
    allowNull: false,
    type: DataTypes.INTEGER,
    references: {
      model: {tableName: "OrganizationId"},
      key: "id"
    }
  },
  createdAt: {
    allowNull: false,
    type: Sequelize.DATE,
  },
  updatedAt: {
    allowNull: false,
    type: Sequelize.DATE,
  },
});

//Risk.belongsTo(Organization);
//Organization.hasMany(Risk);

//Probability.belongsTo(Risk);
Risk.belongsTo(Probability);
Risk.belongsTo(Impact);

module.exports = Risk;
