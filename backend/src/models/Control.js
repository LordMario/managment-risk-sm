const { Sequelize, DataTypes } = require("sequelize");
const sequilize = require("../db");

const Control = sequilize.define("Control", {
    id: {
        primaryKey: true, 
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false
      },
      nombre: {
        allowNull: false,
        type: DataTypes.STRING
      },
      description:{
        allowNull: false,
        type: DataTypes.TEXT
      },
      frequency:{
        allowNull: false,
        type: DataTypes.INTEGER
      },
      compliance:{
        allowNull: false,
        type: DataTypes.INTEGER
      },
      documentation:{
        allowNull: false,
        type: DataTypes.INTEGER        
      },
      resposible:{
        allowNull: false,
        type: DataTypes.INTEGER        
      },
      evidence:{
        allowNull: false,
        type: DataTypes.INTEGER        
      },
      efficiency:{
        allowNull: false,
        type: DataTypes.DOUBLE        
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
});

module.exports = Control;
