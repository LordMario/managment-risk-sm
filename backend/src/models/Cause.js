const { Sequelize, DataTypes } = require("sequelize");
const sequilize = require("../db");

const Cause = sequilize.define("Cause", {
  id: {
    primaryKey: true,
    autoIncrement: true,
    type: DataTypes.INTEGER,
    allowNull: false,
  },
  causeDescription: {
    allowNull: false,
    type: DataTypes.TEXT,
  },
  riskId: {
    allowNull: false,
    type: DataTypes.INTEGER,
    references: {
      model: { tableName: "Risks" },
      key: "id",
    },
  },
  createdAt: {
    allowNull: false,
    type: Sequelize.DATE,
  },
  updatedAt: {
    allowNull: false,
    type: Sequelize.DATE,
  },
});

module.exports = Cause;
