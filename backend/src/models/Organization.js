const { Sequelize, DataTypes } = require("sequelize");
const sequilize = require("../db");
const User = require("../models/User");
const Impact = require("../models/parametization/Impact");
const Probability = require("../models/parametization/Probability");

const Organization = sequilize.define("Organization", {
  id: {
    primaryKey: true,
    autoIncrement: true,
    type: DataTypes.INTEGER,
    allowNull: false,
  },
  organizationName: {
    allowNull: false,
    type: DataTypes.STRING,
  },
  address: {
    allowNull: true,
    type: DataTypes.TEXT,
  },
  industry: {
    allowNull: true,
    type: DataTypes.STRING,
  },
  mission: {
    allowNull: true,
    type: DataTypes.TEXT,
  },
  vision: {
    allowNull: true,
    type: DataTypes.TEXT,
  },
  userId: {
    allowNull: false,
    type: DataTypes.INTEGER,
    references: {
      model: {
        tableName: "Users",
      },
      key: "id",
    },
  },
  contactNumber: {
    allowNull: true.valueOf,
    type: DataTypes.STRING(15)
  },
  createdAt: {
    allowNull: false,
    type: Sequelize.DATE,
  },
  updatedAt: {
    allowNull: false,
    type: Sequelize.DATE,
  },
});

User.hasOne(Organization);
Organization.belongsTo(User);

//Organization.hasMany(Impact);
//Impact.belongsTo(Organization);

//Organization.hasMany(Probability);
//Probability.belongsTo(Organization);

module.exports = Organization;
