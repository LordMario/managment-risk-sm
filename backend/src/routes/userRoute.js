const express = require("express");
const router = express.Router();

//middlewares
const auth = require('../middlewares/authMiddleware');

//controller
const userController = require("../controllers/UserController");

//users
router.get('/api/v1/users', auth, userController.index);
router.get('/api/v1/user/:id', userController.show);
router.post('/api/v1/user', userController.create);
router.put('/api/v1/user/:id', userController.update);
router.delete('/api/v1/user/:id', userController.delete);

module.exports = router;