const express = require('express');
const router = express.Router();

//controller
const OrganizationController = require('../../controllers/OrganizationController');

router.get('/api/v1/organizations', OrganizationController.index);
router.get('/api/v1/admin/organization/:id', OrganizationController.show);
router.post('/api/v1/admin/organization', Auth, OrganizationController.create);
router.put('/api/v1/admin/organization/:id', Auth, OrganizationController.update);

module.exports = router;