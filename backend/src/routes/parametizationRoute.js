const express = require('express');
const router = express.Router();

//controller
const parametizationController = require('../controllers/parametizationController');

/* router.get('/api/v1/impacts', ImpactController.index);
router.post('/api/v1/impact', ImpactController.create);
router.put('/api/v1/impact/:id', ImpactController.update); */

router.get('/api/v1/parametization/:type', parametizationController.index);
router.post('/api/v1/parametization/:type', parametizationController.create);
router.put('/api/v1/parametization/:type/:id', parametizationController.update);

//parametezation default
router.get('/api/v1/parametization-default', parametizationController.getParametizationDefault);
router.post('/api/v1/organization/:id/parametization-default', parametizationController.createParametizationDefault);

module.exports = router;