const express = require('express');
const router = express.Router();

//middleware
const Auth = require('../../middlewares/authMiddleware');

//controller
const OrganizationController = require('../../controllers/OrganizationController');

router.get('/api/v1/organization/:id', Auth, OrganizationController.show);
router.post('/api/v1/organization', Auth, OrganizationController.create);
router.put('/api/v1/organization/:id', OrganizationController.update);

router.get('/api/v1/organization/:id/parametization/:type', OrganizationController.getParametizationType);
router.get('/api/v1/organization/:id/parametization', OrganizationController.getParametization);

module.exports = router;