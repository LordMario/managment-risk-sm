const express = require('express');
const router = express.Router();

//middlewares
const AuthMiddleware =  require('../../middlewares/authMiddleware');

//controller
const RiskController = require('../../controllers/RiskController');

router.get('/api/v1/organization/risk/:id', RiskController.show);
router.post('/api/v1/organization/risk', RiskController.create);
router.put('/api/v1/organization/risk/:id', RiskController.update);
/* router.delete('api/v1/organization/:id/risk/:id', RiskController.delete); */

router.get('/api/v1/organization/:id/risks', RiskController.getRisk);

//router.use('/api/v1', AuthMiddleware);

module.exports = router;


