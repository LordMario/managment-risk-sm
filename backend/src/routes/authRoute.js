const express = require("express");
const router = express.Router();

//controller
const authController = require("../controllers/AuthController");

router.post('/api/v1/login', authController.login);
router.post('/api/v1/signup', authController.signup);


module.exports = router;