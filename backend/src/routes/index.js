const express = require('express');
const router = express.Router();

//middlewares
const AuthMiddleware =  require('../middlewares/authMiddleware');

//routes
router.use(require('./authRoute'));
router.use(require('./userRoute'));
router.use(require('./organization/RiskRoute'));
router.use(require('./organization/organizationRoute'));
router.use(require('./parametizationRoute'));
router.use(require('./risk/causeRoute'));
router.use(require('./risk/effectRoute'));

router.get('/test', (req, res)=>{
    res.json('test');
});

module.exports = router;