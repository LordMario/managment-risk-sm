const express = require('express');
const router = express.Router();

//controller
const EffectController = require('../../controllers/EffectController');

//middleware
const Auth = require('../../middlewares/authMiddleware');

//router.get('/api/v1/risk/causes', CauseEffectController.index);
//router.get('/api/v1/risk/effect/:id', CauseEffectController.show);
router.post('/api/v1/risk/effects', EffectController.create);
router.put('/api/v1/risk/effect/:id', EffectController.update);
router.get('/api/v1/risk/:id/effects', EffectController.getEffects);

module.exports = router;