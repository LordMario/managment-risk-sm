const express = require('express');
const router = express.Router();

//controller
const CauseController = require('../../controllers/CauseController');

//middleware
const Auth = require('../../middlewares/authMiddleware');

//router.get('/api/v1/risk/causes', CauseEffectController.index);
//router.get('/api/v1/risk/cause/:id', CauseEffectController.show);
router.post('/api/v1/risk/causes', CauseController.create);
router.put('/api/v1/risk/cause/:id', CauseController.update);
router.get('/api/v1/risk/:id/causes', CauseController.getCauses);

module.exports = router;