const express = require('express');
const router = express.Router();

//controller
const ControlController = require('../../controllers/ControlController');

//middleware
const Auth = require('../../middlewares/authMiddleware');

router.post('/api/v1/risk/control', ControlController.create);
router.put('/api/v1/risk/control/:id', ControlController.update);
router.get('/api/v1/risk/:id/controls', ControlController.getControls);

module.exports = router;