import { Component, OnInit } from '@angular/core';
import { RiesgoService } from 'app/services/riesgo.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
@Component({
  selector: 'app-typography',
  templateUrl: './typography.component.html',
  styleUrls: ['./typography.component.css']
})
export class TypographyComponent implements OnInit {

  constructor(private _riesgo: RiesgoService, private formBuilder: FormBuilder) {}
  impacto     :any;
  probabilidad:any;
  formProbabilidad: FormGroup;
  formImpacto:  FormGroup;
  ngOnInit() {
    this._riesgo.getProbabilidadImpacto().subscribe((data:any)=>{
      this.probabilidad=data.data.Probabilities;
      this.impacto=data.data.Impacts;
    })
  }
  formulario(){
    this.formProbabilidad = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
    this.formProbabilidad = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

}
